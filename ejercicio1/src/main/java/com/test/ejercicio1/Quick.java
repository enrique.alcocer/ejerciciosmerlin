package com.test.ejercicio1;

import java.math.BigInteger;

public class Quick {
    public static void main(String[] args) throws IllegalArgumentException{
        BigInteger sumOfFactorialDigits = getSumOfFactorialDigits(10);
        BigInteger sumOfFactorialDigits1 = getSumOfFactorialDigits(100);
        System.out.println("10! sum of digits: " + sumOfFactorialDigits);
        System.out.println("100! sum of digits: " + sumOfFactorialDigits1);
        System.out.println("last ten digits of the series :" + lastTenDigits(sumOfFactorialDigits.intValue()));
        System.out.println("last ten digits of the series :" + lastTenDigits(sumOfFactorialDigits.intValue()));
    }

    private static BigInteger getSumOfFactorialDigits(int number) {
        BigInteger factorialResult = factorial(number);
        BigInteger sumOfDigits=BigInteger.ZERO;
        while (!factorialResult.equals(BigInteger.ZERO)){
            factorialResult = factorialResult.divide(BigInteger.TEN);
            sumOfDigits = sumOfDigits.add(factorialResult.mod(BigInteger.TEN));
        }
        return sumOfDigits;
    }

    private static BigInteger factorial(int number) {
        BigInteger factorial = BigInteger.valueOf(number);
        for(int i = number; i > 1; i--){
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        return factorial;
    }

    private static String lastTenDigits(int number) throws IllegalArgumentException{
        if(String.valueOf(number).length()<10){
            throw new IllegalArgumentException("Number dont have more than 10 digits");
        }
        BigInteger series = BigInteger.ZERO;
        for (int i = 1; i <= number; i++) {
            series = series.add(BigInteger.valueOf(i).pow(i));
        }
        String lastTenDigits = series.toString();
        return lastTenDigits.substring(lastTenDigits.length() - 10);
    }
}
