package com.test.ejercicio1;

import java.util.Scanner;

public class Factorial {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int index = in.nextInt();
        if(index<=0) throw new IllegalArgumentException("Value not valid");

        int factorial = findFactorial(index, index);
        System.out.println("factorial:"+ factorial);
        String strFactorial = String.valueOf(factorial);
        int sum = 0;
        for (int i = 0; i < strFactorial.length(); i++) {
            sum += Integer.parseInt(String.valueOf(strFactorial.charAt(i)));

        }

        System.out.println("Sum factorial is: " + sum);

    }

    private static int findFactorial(int index, int value) {

        index--;
        if (index <= 1) return value;
        value *= index;


        return findFactorial(index, value);
    }


}
